import * as React from 'react';
import { Button, ChakraProvider, Flex, Text, VStack } from '@chakra-ui/react';
import { createSlice, configureStore } from '@reduxjs/toolkit';
import { Provider, useDispatch, useSelector } from 'react-redux';

// Membuat reducer
const ticTacToe = createSlice({
  name: 'ticTacToe',
  initialState: {
    squares: Array(9).fill(null),
    winner: null,
    nextValue: 'X',
    status: 'Giliran: X',
  },
  reducers: {
    selectSquare(state, action) {
      if (!state.winner && !state.squares[action.payload]) {
        const newSquares = [...state.squares];
        newSquares[action.payload] = calculateNextValue(state.squares);
        const winner = calculateWinner(newSquares);
        const nextValue = calculateNextValue(newSquares);
        const status = calculateStatus(winner, newSquares, nextValue);
        return {
          squares: newSquares,
          winner,
          status
        }
      }
    },
    restart(state, action) {
      return {
        squares: Array(9).fill(null),
        winner: null,
        nextValue: 'X',
        status: 'Giliran: X',
      };
    },
  }
});

// Aksi
export const { selectSquare, restart } = ticTacToe.actions;

// Store
const store = configureStore({
  reducer: ticTacToe.reducer
});

function Board() {
  const { status, squares } = useSelector(state => state);
  const dispatch = useDispatch();

  function selectSquareHandler(squareIndex) {
    dispatch(selectSquare(squareIndex));
  }

  function renderSquare(i) {
    return (
      <Button
        w="100px"
        h="100px"
        m='5px'
        color='yellow'
        fontSize='50px'
        variant="outline"
        borderWidth="2px"
        borderColor="gray"
        backgroundColor='salmon'
        onClick={() => selectSquareHandler(i)}
      >
        {squares[i]}
      </Button>
    );
  }

  return (
    <VStack>
      <Text fontSize="200%" color='brown' fontWeight="bold" >{status}</Text>
      <Flex>
        {renderSquare(0)}
        {renderSquare(1)}
        {renderSquare(2)}
      </Flex>
      <Flex>
        {renderSquare(3)}
        {renderSquare(4)}
        {renderSquare(5)}
      </Flex>
      <Flex>
        {renderSquare(6)}
        {renderSquare(7)}
        {renderSquare(8)}
      </Flex>
      <Button
        colorScheme="red"
        mt="4"
        onClick={() => dispatch(restart())}
      >
        Ulangi
      </Button>
    </VStack>
  );
}

function Game() {
  return (
    <Flex
      justify="center"
      align="center"
      minH="100vh"
      bg="green.200"
    >
      <div>
        <Board />
      </div>
    </Flex>
  );
}

function calculateStatus(winner, squares, nextValue) {
  return winner
    ? `Pemenangnya Adalah: ${winner}`
    : squares.every(Boolean)
      ? `Hasil Seimbang`
      : `Giliran: ${nextValue}`;
}

function calculateNextValue(squares) {
  return squares.filter(Boolean).length % 2 === 0 ? 'X' : 'O';
}

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

function App() {
  return (
    <ChakraProvider>
      <Provider store={store}>
        <Game />
      </Provider>
    </ChakraProvider>
  );
}

export default App;
